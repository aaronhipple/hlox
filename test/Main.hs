module Main where

import           System.Environment             ( getArgs
                                                , lookupEnv
                                                )

import           Test.HSpec.JUnit
import           Test.Hspec.Runner
import qualified Spec

main = do
  config <- readConfig defaultConfig =<< getArgs
  inCI   <- isRunningInCI

  if inCI
    then do
      runJUnitSpec Spec.spec ("build", "hlox-text") $ config
      return ()
    else hspecWith config Spec.spec

isRunningInCI = do
  ci <- lookupEnv "CI"
  case ci of
    (Just val) -> return $ val == "true"
    _          -> return False
