-- |Test.ScannerSpec tests the Scanner module

module Test.ScannerSpec
  ( spec
  )
where

import           Test.Hspec
import           Test.QuickCheck

import           Error                          ( Error(..) )
import qualified Scanner                        ( scanTokens )
import qualified ScannerNaive                   ( scanTokens )
import           Token                          ( Token(..)
                                                , TokenType(..)
                                                )

spec = describe "Scanners" $ do

  describe "ScannerNaive.scanTokens" $ do
    testSimpleProgram ScannerNaive.scanTokens

  describe "Scanner.scanTokens" $ do
    testSimpleProgram Scanner.scanTokens

  describe "Both Scanners" $ do
    it "should both produce equivalent output for \"a\\9317\"" $ do
      Scanner.scanTokens "a\9317" `shouldBe` ScannerNaive.scanTokens "a\9317"

    it "should both produce equivalent output for \"//\"" $ do
      Scanner.scanTokens "//" `shouldBe` ScannerNaive.scanTokens "//"

    it "should both produce equivalent output for \"ora\"" $ do
      Scanner.scanTokens "ora" `shouldBe` ScannerNaive.scanTokens "ora"

    it "should both produce equivalent output for \"==a\"" $ do
      Scanner.scanTokens "==a" `shouldBe` ScannerNaive.scanTokens "==a"

    it "should both produce equivalent output" $ property
      (\source ->
        source
          /= "\"\n\"" -- Scanner has better behavior here, I think.
          && Scanner.scanTokens source
          == ScannerNaive.scanTokens source
      )


testSimpleProgram s = do
  it "scans a simple program correctly" $ do
    s "foo = \"bar\"" `shouldBe` Right
      [ Token { token = IDENTIFIER "foo", lexeme = "foo", Token.line = 1 }
      , Token { token = EQUAL, lexeme = "=", Token.line = 1 }
      , Token { token = STRING "bar", lexeme = "\"bar\"", Token.line = 1 }
      , Token { token = EOF, lexeme = "\0", Token.line = 1 }
      ]

  it "scans a multi-line program correctly" $ do
    s "foo = \"bar\"\n// Comment here\n(2 + 3) / 4" `shouldBe` Right
      [ Token { token = IDENTIFIER "foo", lexeme = "foo", Token.line = 1 }
      , Token { token = EQUAL, lexeme = "=", Token.line = 1 }
      , Token { token = STRING "bar", lexeme = "\"bar\"", Token.line = 1 }
      , Token { token = LEFT_PAREN, lexeme = "(", Token.line = 3 }
      , Token { token = NUMBER 2.0, lexeme = "2", Token.line = 3 }
      , Token { token = PLUS, lexeme = "+", Token.line = 3 }
      , Token { token = NUMBER 3.0, lexeme = "3", Token.line = 3 }
      , Token { token = RIGHT_PAREN, lexeme = ")", Token.line = 3 }
      , Token { token = SLASH, lexeme = "/", Token.line = 3 }
      , Token { token = NUMBER 4.0, lexeme = "4", Token.line = 3 }
      , Token { token = EOF, lexeme = "\0", Token.line = 3 }
      ]

  it "scans keyword expressions correctly" $ do
    let source = unlines
          [ "fun doThings(param) {"
          , "  while (a < 10) {"
          , "    print a;"
          , "  }"
          , "}"
          ]
    s source `shouldBe` Right
      [ Token FUN                     "fun"      1
      , Token (IDENTIFIER "doThings") "doThings" 1
      , Token LEFT_PAREN              "("        1
      , Token (IDENTIFIER "param")    "param"    1
      , Token RIGHT_PAREN             ")"        1
      , Token LEFT_BRACE              "{"        1
      , Token WHILE                   "while"    2
      , Token LEFT_PAREN              "("        2
      , Token (IDENTIFIER "a")        "a"        2
      , Token LESS                    "<"        2
      , Token (NUMBER 10.0)           "10"       2
      , Token RIGHT_PAREN             ")"        2
      , Token LEFT_BRACE              "{"        2
      , Token PRINT                   "print"    3
      , Token (IDENTIFIER "a")        "a"        3
      , Token SEMICOLON               ";"        3
      , Token RIGHT_BRACE             "}"        4
      , Token RIGHT_BRACE             "}"        5
      , Token EOF                     "\0"       6
      ]


  it "scans numerical expressions correctly" $ do
    s "-123 * (45.67)" `shouldBe` Right
      [ Token MINUS          "-"     1
      , Token (NUMBER 123.0) "123"   1
      , Token STAR           "*"     1
      , Token LEFT_PAREN     "("     1
      , Token (NUMBER 45.67) "45.67" 1
      , Token RIGHT_PAREN    ")"     1
      , Token EOF            "\0"    1
      ]

  it "errors on unclosed strings" $ do
    s "\"foo" `shouldBe` Left Error { Error.line = 1
                                    , location   = ""
                                    , message    = "expecting end of string"
                                    }
