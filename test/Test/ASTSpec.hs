-- |ASTSpec tests AST utils

module Test.ASTSpec
  ( spec
  )
where

import           Test.Hspec

import           AST
import           Token

spec = describe "AST" $ do

  describe "Expression" $ do
    describe "Show instances" $ do
      it "correctly prints an AST" $ do
        show
            (BinaryExpression
              (UnaryExpression
                (Token MINUS "-" 1)
                (LiteralExpression (Token (NUMBER 123.0) "123" 1))
              )
              (Token STAR "*" 1)
              (GroupingExpression
                (LiteralExpression (Token (NUMBER 45.67) "45.67" 1))
              )
            )
          `shouldBe` "(* (- 123) (group 45.67))"
