-- |

module Test.ParserSpec
  ( spec
  )
where

import           Test.Hspec

import           AST
import           Token
import           Parser
import           Scanner

spec = describe "Parser" $ do
  describe "parse" $ do
    it "correctly parses \"-123 * (45.67)\"" $ do
      pipeline scanTokens "-123 * (45.67)"
        `shouldBe` (Right "(* (- 123) (group 45.67))")
    it "correctly parses a more complex program" $ do
      pipeline scanTokens complexProgram `shouldBe` (Right complexPrettyPrint)

complexProgram = unlines ["!(3 * 2) +", "125.6 - ", "(32.8 / 0)"]
complexPrettyPrint = "(+ (! (group (* 3 2))) (- 125.6 (group (/ 32.8 0))))"

pipeline scanner source = show <$> (parseTokens =<< scanner =<< pure source)
