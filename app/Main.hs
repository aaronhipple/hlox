module Main where

import           Lib                            ( run )
import           System.Environment             ( getArgs )
import           System.Exit                    ( exitWith
                                                , ExitCode(..)
                                                )
import           System.IO                      ( hFlush
                                                , stdout
                                                )

main :: IO ()
main = getArgs >>= runArgs

-- |runArgs accepts command line arguments and dispatches the appropriate
runArgs :: [String] -> IO ()
runArgs []     = runPrompt
runArgs [path] = runFile path
runArgs _      = printUsage


-- |runPrompt provides REPL (read-eval-print-loop) functionality.
runPrompt :: IO ()
runPrompt = putStr "λ " >> hFlush stdout >> getLine >>= runAndLoopOrEnd
 where
  runAndLoopOrEnd ""    = pure ()
  runAndLoopOrEnd input = run input >> runPrompt


-- |runFile reads a file at the given path and executes it.
runFile :: String -> IO ()
runFile path = readFile path >>= run


-- |printUsage shows usage information and exits.
printUsage :: IO ()
printUsage = putStrLn "Usage: hlox [script]" >> exitWith (ExitFailure 64)
