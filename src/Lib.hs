-- |Lib contains the run function and not much else.

module Lib
  ( run
  )
where

import           Scanner                        ( scanTokens )
import           Parser                         ( parseTokens )
import           Token                          ( Token(..) )

import           Error                          ( report )

run :: String -> IO ()
run source = either report showResult (parseTokens =<< scanTokens source)
  where showResult = (putStrLn . show)
