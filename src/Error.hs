-- |Errors contains functions for error reporting and handling.

module Error where

import           System.IO                      ( hPutStrLn
                                                , stderr
                                                )

-- |Error represents an error encountered while running a Lox program.
data Error = Error { line :: Int
                   , location :: String
                   , message :: String
                   }
             deriving (Eq)

instance Show Error where
  -- TODO is a Show instance the right way to format this? It's convenient.
  show (Error line location msg) =
    "[line " ++ (show line) ++ "] Error" ++ location ++ ": " ++ msg

-- |report prints an error to stderr
report :: Error -> IO ()
report = (hPutStrLn stderr) . show
