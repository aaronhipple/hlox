-- |

module ParsecUtils
  ( runParsec
  )
where

import           Data.Bifunctor                 ( Bifunctor
                                                , first
                                                )

import           Text.Parsec.Error              ( ParseError
                                                , errorMessages
                                                , messageString
                                                , errorPos
                                                )

import           Error
import           Text.Parsec.Pos                ( sourceLine )

-- |runParsec take a parsec parser and parse function and runs it for Lox.
-- It handles some boilerplate translation of errors for us.
runParsec
  :: Bifunctor p => (t -> [Char] -> a -> p ParseError c) -> t -> a -> p Error c
runParsec p parser = (first fixError) . p parser ""
 where
  fixError :: ParseError -> Error
  fixError e = Error { Error.line = sourceLine $ errorPos e
                     , location   = ""
                     , message    = concat (messageString <$> errorMessages e)
                     }
