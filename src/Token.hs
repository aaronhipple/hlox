-- |Token contains types and utilities for working with tokens.

module Token where

data Token = Token { token :: TokenType
                   , lexeme :: String
                   , line :: Int
                   }
           deriving (Show, Eq)

data TokenType
  = LEFT_PAREN
  | RIGHT_PAREN
  | LEFT_BRACE
  | RIGHT_BRACE
  | COMMA
  | DOT
  | MINUS
  | PLUS
  | SEMICOLON
  | SLASH
  | STAR
  | BANG
  | BANG_EQUAL
  | EQUAL
  | EQUAL_EQUAL
  | GREATER
  | GREATER_EQUAL
  | LESS
  | LESS_EQUAL
  | IDENTIFIER String
  | STRING String
  | NUMBER Double
  | AND
  | CLASS
  | ELSE
  | FALSE
  | FUN
  | FOR
  | IF
  | NIL
  | OR
  | PRINT
  | RETURN
  | SUPER
  | THIS
  | TRUE
  | VAR
  | WHILE
  | EOF
  deriving (Show, Eq)

typeForAlphanumeric :: String -> TokenType
typeForAlphanumeric "and"    = AND
typeForAlphanumeric "class"  = CLASS
typeForAlphanumeric "else"   = ELSE
typeForAlphanumeric "false"  = FALSE
typeForAlphanumeric "for"    = FOR
typeForAlphanumeric "fun"    = FUN
typeForAlphanumeric "if"     = IF
typeForAlphanumeric "nil"    = NIL
typeForAlphanumeric "or"     = OR
typeForAlphanumeric "print"  = PRINT
typeForAlphanumeric "return" = RETURN
typeForAlphanumeric "super"  = SUPER
typeForAlphanumeric "this"   = THIS
typeForAlphanumeric "true"   = TRUE
typeForAlphanumeric "var"    = VAR
typeForAlphanumeric "while"  = WHILE
typeForAlphanumeric s        = IDENTIFIER s
