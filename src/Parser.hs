-- |Parser interprets a stream of tokens and produces an AST.

module Parser
  ( parseTokens
  )
where


import           Text.Parsec.Prim        hiding ( Error
                                                , parse
                                                , token
                                                )
import qualified Text.Parsec.Prim              as Prim
                                                ( parse )
import           Text.Parsec.Combinator
import           Text.Parsec.Pos                ( SourcePos
                                                , setSourceLine
                                                , setSourceColumn
                                                , sourceLine
                                                , sourceColumn
                                                )

import           Error
import           Token                   hiding ( token )
import qualified Token                          ( token )
import           AST
import           ParsecUtils

parseTokens :: [Token] -> Either Error Expression
parseTokens = runParsec Prim.parse expression

expression = equality

equality = leftBinary (token EQUAL_EQUAL <|> token BANG_EQUAL) comparison

comparison = leftBinary
  (token GREATER <|> token GREATER_EQUAL <|> token LESS <|> token LESS_EQUAL)
  addition

addition = leftBinary (token MINUS <|> token PLUS) multiplication

multiplication = leftBinary (token SLASH <|> token STAR) unary

leftBinary opParser expParser =
  join <$> expParser <*> many ((,) <$> opParser <*> expParser)

join l []               = l
join l ((op, r) : next) = BinaryExpression l op (join r next)

unary = UnaryExpression <$> (token MINUS <|> token BANG) <*> unary <|> primary

primary = literal <|> grouping

literal =
  LiteralExpression
    <$> (   satisfy isNumber
        <|> satisfy isString
        <|> token FALSE
        <|> token TRUE
        <|> token NIL
        )

grouping =
  GroupingExpression
    <$> (  token LEFT_PAREN
        *> expression
        <* (token RIGHT_PAREN <?> "Expect ')' after expression.")
        )

token = satisfy . isToken

isToken tokenType t = tokenType == Token.token t

isNumber t = case Token.token t of
  (NUMBER _) -> True
  _          -> False

isString t = case Token.token t of
  (STRING _) -> True
  _          -> False

satisfy f = tokenPrim showToken nextPos testToken
 where
  showToken = show
  testToken t | f t       = Just t
              | otherwise = Nothing
  nextPos pos t _ts = updatePosToken pos t

updatePosToken :: SourcePos -> Token -> SourcePos
updatePosToken p t =
  p `setSourceLine` (Token.line t) `setSourceColumn` newColumn
 where
  newColumn | sourceLine p == Token.line t = oldColumn + 1
            | otherwise                    = 1

  oldColumn = sourceColumn p
