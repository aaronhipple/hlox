-- |ScannerNaive is a transliteration of the craftinginterpreters.com scanner.
-- It is not idiomatic Haskell. I've left it here for comparison purposes and to
-- test that the more idiomatic implementation behaves roughly the same way.

module ScannerNaive
  ( scanTokens
  )
where


import           Data.Char                      ( isAlphaNum
                                                , isAlpha
                                                , isDigit
                                                )
import           Control.Monad.State            ( StateT
                                                , evalStateT
                                                , get
                                                , modify
                                                )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.Trans.Except     ( ExceptT
                                                , runExceptT
                                                )
import           Control.Monad.Identity         ( Identity
                                                , runIdentity
                                                )
import           Control.Monad.Loops            ( whileM_ )

import           Error                          ( Error(..) )
import           Token                          ( Token(..)
                                                , TokenType(..)
                                                , typeForAlphanumeric
                                                )

-- |scanTokens scans source and produces a list of tokens.
scanTokens :: String -> Either Error [Token]
scanTokens source = runScanner scan initialState
 where
  initialState = (ScannerState 0 0 1 source [])
  scan         = do
    whileM_ isNotAtEnd (modify (\s -> s { start = (current s) }) >> scanToken)
    s <- get
    pure $ tokens
      (s { tokens = (tokens s) ++ [Token EOF "\0" (ScannerNaive.line s)] })


data ScannerState = ScannerState { start :: Int
                        , current :: Int
                        , line :: Int
                        , source :: String
                        , tokens :: [Token]
                        }

scanToken :: Scanner Error ()
scanToken = do
  c <- advance
  case c of
    '(' -> addToken LEFT_PAREN
    ')' -> addToken RIGHT_PAREN
    '{' -> addToken LEFT_BRACE
    '}' -> addToken RIGHT_BRACE
    ',' -> addToken COMMA
    '.' -> addToken DOT
    '-' -> addToken MINUS
    '+' -> addToken PLUS
    ';' -> addToken SEMICOLON
    '*' -> addToken STAR
    '!' -> do
      matches <- match '='
      addToken $ if matches then BANG_EQUAL else BANG
    '=' -> do
      matches <- match '='
      addToken $ if matches then EQUAL_EQUAL else EQUAL
    '<' -> do
      matches <- match '='
      addToken $ if matches then LESS_EQUAL else LESS
    '>' -> do
      matches <- match '='
      addToken $ if matches then GREATER_EQUAL else GREATER
    '/' -> do
      matches <- match '/'
      if matches
        then whileM_
          (do
            c     <- peek
            atEnd <- isAtEnd
            pure ((c /= '\n') && (not atEnd))
          )
          advance
        else addToken SLASH
    ' '  -> pure ()
    '\r' -> pure ()
    '\t' -> pure ()
    '\n' -> chompLine
    '"'  -> string
    c
      | isDigit c -> number
      | isAlpha c -> identifier
      | otherwise -> do
        s <- get
        throwError
          (Error (ScannerNaive.line s)
                 ""
                 ("unexpected character `" ++ [c] ++ "`")
          )

advance :: Scanner Error Char
advance = do
  s <- get
  chompChar
  pure $ currentChar s

chompChar :: Scanner Error ()
chompChar = modify (\s -> s { current = (current s + 1) })

chompLine :: Scanner Error ()
chompLine = modify (\s -> s { ScannerNaive.line = (ScannerNaive.line s + 1) })

addToken :: TokenType -> Scanner Error ()
addToken tokenType = do
  s    <- get
  text <- getTextWithPad 0
  modify
    (\s ->
      s { tokens = tokens s ++ [Token tokenType text (ScannerNaive.line s)] }
    )

string :: Scanner Error ()
string = do
  whileM_ inString chompString

  atEnd <- isAtEnd

  if atEnd
    then do
      s <- get
      throwError (Error (ScannerNaive.line s) "" ("expecting end of string"))
    else do
      advance
      value <- getTextWithPad 1
      addToken (STRING value)

 where
  inString = do
    c     <- peek
    atEnd <- isAtEnd
    pure ((c /= '"') && (not atEnd))

  chompString = do
    c <- peek
    if c == '\n' then chompLine else pure ()
    advance

number :: Scanner Error ()
number = do
  whileM_ (peekMatches isDigit) advance

  c  <- peek
  c_ <- peekNext

  if c == '.' && isDigit (c_)
    then do
      advance
      whileM_ (peekMatches isDigit) advance
    else do
      pure ()

  text <- getTextWithPad 0
  addToken $ NUMBER (read text)

peekMatches :: (Char -> Bool) -> Scanner Error Bool
peekMatches predicate = predicate <$> peek

getTextWithPad :: Int -> Scanner Error String
getTextWithPad pad = do
  s <- get
  pure $ take (((current s) - pad) - ((start s) + pad))
              (drop ((start s) + pad) (source s))

identifier :: Scanner Error ()
identifier = do
  whileM_ (peekMatches (\c -> isAlphaNum c)) advance
  text <- getTextWithPad 0
  addToken (typeForAlphanumeric text)

match :: Char -> Scanner Error Bool
match c = do
  atEnd <- isAtEnd
  if atEnd
    then pure False
    else do
      s <- get
      if (currentChar s) /= c
        then pure False
        else do
          chompChar
          pure True

peek :: Scanner Error Char
peek = do
  atEnd <- isAtEnd
  if atEnd then pure '\0' else currentChar <$> get

peekNext :: Scanner Error Char
peekNext = do
  s <- get
  if ((current s) + 1) >= (length $ source s)
    then pure '\0'
    else pure (source s !! ((current s) + 1))

currentChar :: ScannerState -> Char
currentChar s = (source s) !! (current s)

isNotAtEnd :: Scanner Error Bool
isNotAtEnd = not <$> isAtEnd

isAtEnd :: Scanner Error Bool
isAtEnd = do
  s <- get
  pure $ (current s) == (length $ source s)

type ScannerT e m a = StateT ScannerState (ExceptT e m) a

runScannerT :: (Monad m) => ScannerT e m a -> ScannerState -> m (Either e a)
runScannerT m = runExceptT . evalStateT m

type Scanner e a = ScannerT e Identity a

runScanner m = runIdentity . runScannerT m
