-- |Scanner implements a parsec-based scanner for lox

module Scanner
  ( scanTokens
  )
where

import           Data.Functor                   ( ($>) )
import           Data.Bifunctor                 ( first )
import           Text.Parsec             hiding ( Error
                                                , eof
                                                , spaces
                                                , char
                                                , string
                                                , unexpected
                                                )
import qualified Text.Parsec                   as P
import           Text.Parsec.Error              ( errorMessages
                                                , messageString
                                                )

import           Error                          ( Error(..) )
import           Token                          ( Token(..)
                                                , TokenType
                                                )
import qualified Token
import           ParsecUtils

-- |scanTokens scans an input stream of characters to produce list of tokens.
scanTokens :: String -> Either Error [Token]
scanTokens = runParsec parse source

-- |source is the primary entry point for our parser.
-- It combines `spaces` and `comment` (which ignore any productions) with
-- `token` (which produces a list of tokens) and finally must match an `eof`
-- for a valid parse.
source =
  (++) <$> (concat <$> many (spaces <|> comment <|> Scanner.token)) <*> eof

-- |comment matches comments, e.g. // any string following two slashes
-- It first must match two slashes, then it consumes all input until a newline
-- is reached. It produces no results.
comment :: Monad m => ParsecT String u m [Token]
comment =
  try (P.string "//") *> manyTill anyChar ((P.char '\n' $> ()) <|> P.eof) $> []

-- |spaces matches whitespace, e.g. \t \n
-- It produces no results.
spaces :: Monad m => ParsecT String u m [Token]
spaces = (P.char ' ' <|> P.char '\t' <|> P.char '\r' <|> P.char '\n') $> []

-- |eof matches the end of the file.
eof :: Monad m => ParsecT String u m [Token]
eof = do
  P.eof
  position <- getPosition
  pure
    [ Token { Token.token = Token.EOF
            , Token.line  = sourceLine position
            , lexeme      = "\0"
            }
    ]

-- |token is a parser that matches any valid Lox token.
-- It keeps track of position using Parsec's internals, and then produces
-- partially-constructed instances of `Token`, which are then finally given a
-- line at the end.
token :: Monad m => ParsecT String u m [Token]
token = do
  position <- getPosition
  let lineNumber = sourceLine position
  t <-
    string
    <|> number
    <|> word "and"    Token.AND
    <|> word "class"  Token.CLASS
    <|> word "else"   Token.ELSE
    <|> word "false"  Token.FALSE
    <|> word "for"    Token.FOR
    <|> word "fun"    Token.FUN
    <|> word "if"     Token.IF
    <|> word "nil"    Token.NIL
    <|> word "or"     Token.OR
    <|> word "print"  Token.PRINT
    <|> word "return" Token.RETURN
    <|> word "super"  Token.SUPER
    <|> word "this"   Token.THIS
    <|> word "true"   Token.TRUE
    <|> word "var"    Token.VAR
    <|> word "while"  Token.WHILE
    <|> identifier
    <|> chars "!=" Token.BANG_EQUAL
    <|> chars "<=" Token.LESS_EQUAL
    <|> chars ">=" Token.GREATER_EQUAL
    <|> chars "==" Token.EQUAL_EQUAL
    <|> char '(' Token.LEFT_PAREN
    <|> char ')' Token.RIGHT_PAREN
    <|> char '{' Token.LEFT_BRACE
    <|> char '}' Token.RIGHT_BRACE
    <|> char ',' Token.COMMA
    <|> char '.' Token.DOT
    <|> char '-' Token.MINUS
    <|> char '+' Token.PLUS
    <|> char ';' Token.SEMICOLON
    <|> char '*' Token.STAR
    <|> char '/' Token.SLASH
    <|> char '!' Token.BANG
    <|> char '=' Token.EQUAL
    <|> char '>' Token.GREATER
    <|> char '<' Token.LESS
    -- if no other parser has matched, `unexpected` produces a useful error.
    <|> unexpected
  pure $ map (\t -> t lineNumber) t

-- |string is a parser that matches Lox strings, e.g. "hello world" ""
-- It first matches a quote, then parses content and returns that results.
string :: Monad m => ParsecT String u m [Int -> Token]
string = do
  P.char '"'
  content <- try (manyTill anyChar (P.char '"')) <?> "expecting end of string"
  pure [Token (Token.STRING content) ("\"" ++ content ++ "\"")]

-- |number is a parser that matches Lox numbers, e.g. 4 3.14159
-- It first attempts to match a decimal number. If it fails, it then attepts to
-- match a natural number.
number :: Monad m => ParsecT String u m [Int -> Token]
number = do
  digits <-
    try
        (do
          whole      <- many1 digit
          decimal    <- P.char '.'
          fractional <- many1 digit
          pure $ whole ++ (decimal : fractional)
        )
      <|> many1 digit
  pure [Token (Token.NUMBER (read digits)) digits]

-- |identifier is a parser that matches Lox identifiers, e.g. a12345 foobar
-- It first matches a letter, then any sequence of alphanumeric characters, then
-- it joins the two matches into a single string and produces a
identifier :: Monad m => ParsecT String u m [Int -> Token]
identifier = build <$> ((:) <$> letter <*> many alphaNum)
  where build lexeme = [Token (Token.IDENTIFIER lexeme) lexeme]

-- |char matches single-character tokens, e.g. { + -
-- It accepts the character and the resulting TokenType as inputs, and produces
-- a parser to match that character and produce a partial Token.
char :: Monad m => Char -> TokenType -> ParsecT String u m [Int -> Token]
char c tokenType = build <$> P.char c where build c = [Token tokenType [c]]

-- |char matches multi-character tokens, e.g. <= where if
-- It accepts characters and the resulting TokenType as inputs, and produces
-- a parser to match those characters and produce a partial Token.
chars :: Monad m => String -> TokenType -> ParsecT String u m [Int -> Token]
chars cs tokenType = build <$> try (P.string cs)
  where build lexeme = [Token tokenType lexeme]

-- |char matches multi-character tokens, e.g. <= where if
-- It accepts characters and the resulting TokenType as inputs, and produces
-- a parser to match those characters and produce a partial Token.
word :: Monad m => String -> TokenType -> ParsecT String u m [Int -> Token]
word cs tokenType = try $ chars cs tokenType <* notFollowedBy alphaNum

-- |unexpected produces an error for the next input character.
-- It is a "utility" parser for use at the end of a list of alternatives. It
-- produces a message indicating that no valid parse remains (and this character
-- was found instead.)
unexpected :: Monad m => ParsecT String u m a
unexpected = anyChar >>= build
  where build c = P.unexpected ("unexpected character `" ++ [c] ++ "`")
