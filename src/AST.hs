-- |AST defines types and utilities for working with an AST

module AST where

import           Data.List                      ( intercalate )

import           Token

data Expression
  = LiteralExpression Token
  | GroupingExpression Expression
  | UnaryExpression Token Expression
  | BinaryExpression Expression Token Expression

instance Show Expression where
  show (LiteralExpression  literal) = lexeme literal
  show (GroupingExpression exp    ) = paren ["group", show exp]
  show (UnaryExpression op exp    ) = paren [lexeme op, show exp]
  show (BinaryExpression leftExp op rightExp) =
    paren [lexeme op, show leftExp, show rightExp]

paren :: [String] -> String
paren xs = "(" ++ intercalate " " xs ++ ")"
